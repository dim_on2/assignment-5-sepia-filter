#include "../lib/include/file_handler.h"
#include "../lib/include/image.h"
#include "../lib/include/print_info.h"
#include "../lib/include/rotate.h"
#include "../lib/include/transform_bmp.h"
#include "sepia.h"
#include <stdint.h>
#include <stdio.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

static enum status_io closing( FILE* const f1 ){
    enum status_io c_stat1 = file_close( f1 );
    if ( c_stat1 != CLOSE_OK ) println_info( io_message[c_stat1], stderr ); 
    return c_stat1;
}

static void dealloc( struct image i1 ){    
    clear_picture_memory( i1 );
}

static void cleaning( FILE* const f1, FILE * const f2, struct image i1 ){
    dealloc( i1 );
    closing( f1 );
    closing( f2 );
}

static long get_time(){
  struct rusage r = {0};
  return getrusage(RUSAGE_SELF, &r ), r.ru_utime.tv_sec * 1000000L + r.ru_utime.tv_usec;
}

typedef void (filter) ( struct image* img );

static int execute( char * f_1, char * f_2, filter ftr, size_t * const time ){
  long start = get_time();

  FILE *file_for_reading = NULL, *file_for_writing = NULL;

  enum status_io o_stat1 = file_open( &file_for_reading, f_1, "r" );
  if ( o_stat1 != OPEN_OK ) return println_info( io_message[o_stat1], stderr ), 1;

  enum status_io o_stat2 = file_open( &file_for_writing, f_2, "w" );
  if ( o_stat2 != OPEN_OK ) return println_info( io_message[o_stat2], stderr ), closing( file_for_reading ), 1; 

  struct image picture = {0};

  enum read_status read_status = from_bmp( file_for_reading, &picture );
  if ( read_status != READ_OK ) return println_info( output_message[read_status], stderr ), cleaning( file_for_reading, file_for_writing, picture ), 1;

  ftr( &picture );

  enum write_status write_status = to_bmp( file_for_writing, &picture );
  if ( write_status != WRITE_OK ) return println_info( output_message[write_status], stderr ), cleaning( file_for_reading, file_for_writing, picture ), 1;

  enum status_io c_stat1 = closing( file_for_reading ), c_stat2 = closing( file_for_writing );

  dealloc( picture );

  long end = get_time();

  *time = end - start;

  return ( c_stat1 != CLOSE_OK || c_stat2 != CLOSE_OK ) ? printf( "Something went wrong by closing file. \n"), 1 : 0;
}

int main( int argc, char** args ) {
  print_info( "Sepia filter starts...\n ", stderr );
  if ( argc != 4 ) return print_info( "Not valid args. Example: main <source-pic> <dest_pic_c> <dest_pic_asm>", stderr ), 1;
  size_t array_array_long[2][100] = {{0}, {0}};
  for ( size_t i = 0; i < 100; i++ ){
    if (execute( args[1], args[2], sepia_filter, array_array_long[0] + i ) == 1) return 1;
    if (execute( args[1], args[3], sepia_filter_asm_impl, array_array_long[1] + i ) == 1) return 1;  
  }
  for ( size_t i = 1; i < 100; array_array_long[0][0] += array_array_long[0][i], i++ );
  for ( size_t i = 1; i < 100; array_array_long[1][0] += array_array_long[1][i], i++ );
  printf( "Average time c algoritm: %zu\n", array_array_long[0][0] / 100 );
  printf( "Average time asm algoritm: %zu\n", array_array_long[1][0] / 100 );
  return 0;
}
