%macro clear_regs 0
  pxor xmm0, xmm0
  pxor xmm1, xmm1
  pxor xmm2, xmm2
%endmacro

%macro init_matrics_sepia 0
  mov rax, xmm_3
  movdqa xmm3, [rax]
  mov rax, xmm_4
  movdqa xmm4, [rax]
  mov rax, xmm_5
  movdqa xmm5, [rax]
%endmacro

%macro read_data 2
  pinsrb xmm0, [rdi+%1], 0 
  pinsrb xmm1, [rdi+%1+1], 0 
  pinsrb xmm2, [rdi+%1+2], 0 

  pinsrb xmm0, [rdi+%1+3], 4
  pinsrb xmm1, [rdi+%1+4], 4
  pinsrb xmm2, [rdi+%1+5], 4

  %ifnum %2
    %if %2 == 0
      shufps xmm0, xmm0, 0b01000000
      shufps xmm1, xmm1, 0b01000000
      shufps xmm2, xmm2, 0b01000000
    %elif %2 == 4
      shufps xmm0, xmm0, 0b01010000
      shufps xmm1, xmm1, 0b01010000
      shufps xmm2, xmm2, 0b01010000
    %elif %2 == 8
      shufps xmm0, xmm0, 0b01010100
      shufps xmm1, xmm1, 0b01010100
      shufps xmm2, xmm2, 0b01010100
    %else
      %error "not supported number"
    %endif
  %else
    %error "not number"
  %endif

  cvtdq2ps xmm0, xmm0
  cvtdq2ps xmm1, xmm1
  cvtdq2ps xmm2, xmm2
%endmacro

%macro perform_sepia 0
  mulps xmm0, xmm3
  mulps xmm1, xmm4
  mulps xmm2, xmm5
  addps xmm0, xmm2
  addps xmm0, xmm1
  cvtps2dq xmm0, xmm0
  pminsd xmm0, [m_val]
%endmacro

%macro write_data 1
  pextrb [ans+%1],   xmm0,   0
  pextrb [ans+%1+1], xmm0,   4
  pextrb [ans+%1+2], xmm0,   8
  pextrb [ans+%1+3], xmm0,   12
%endmacro

%macro transform_matrics 0
  shufps xmm3, xmm3, 0b01001001 ; .131, .168, .189, .131 -> .189, .131, .168, .189 -> .168, .189, .131, .168
  shufps xmm4, xmm4, 0b01001001 
  shufps xmm5, xmm5, 0b01001001
%endmacro

%macro reverse_registers 0
  shufps xmm0, xmm0, 0b00011011
  shufps xmm1, xmm1, 0b00011011
  shufps xmm2, xmm2, 0b00011011
%endmacro

%macro handle_four_bytes 2
  read_data %1, %2
  perform_sepia
  write_data %2
  transform_matrics
  clear_regs
%endmacro

section .data
ans:  dd 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 

section .rodata
align 16
xmm_3:  dd    0.131,       0.168,       0.189,       0.131,   
xmm_4:  dd    0.543,       0.686,       0.769,       0.543,   
xmm_5:  dd    0.272,       0.349,       0.393,       0.272,   
;             b*xmm3      b*xmm3      b*xmm3      b*xmm3
;           + g*xmm4    + g*xmm4    + g*xmm4    + g*xmm4  
;           + r*xmm5    + r*xmm5    + r*xmm5    + r*xmm5  
;             ------      ------      ------      ------
;            b_new_1     g_new_1     r_new_1     b_new_2 
m_val:  dd    0xFF,       0xFF,       0xFF,       0xFF 

section .text
global sepia_filter_asm
sepia_filter_asm:
  ; rdi - pointer to initial data
  ; initial data is like this: b1 g1 r1  b2 g2 r2  b3 g3 r3  b4 g4 r4
  init_matrics_sepia
  clear_regs
  handle_four_bytes 0, 0
  handle_four_bytes 3, 4
  handle_four_bytes 6, 8
  %assign i 0
  %rep 12
    pinsrb xmm0, [ans + i], 0
    pextrb [rdi + i], xmm0, 0
    %assign i i+1
  %endrep
  ret