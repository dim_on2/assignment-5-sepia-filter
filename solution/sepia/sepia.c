#include "sepia.h"
#include "../lib/include/print_info.h"


static unsigned char sat( double x) {
  if (x < 256) return (unsigned char) x; return 255;
}

static void sepia_one( struct pixel* const pixel ) {
  static const double c[3][3] = {
    { .393, .769, .189 },
    { .349, .686, .168 },
    { .272, .543, .131 } };
  struct pixel const old = *pixel;
  pixel->r = sat( old.r * c[0][0] + old.g * c[0][1] + old.b * c[0][2] );
  pixel->g = sat( old.r * c[1][0] + old.g * c[1][1] + old.b * c[1][2] );
  pixel->b = sat( old.r * c[2][0] + old.g * c[2][1] + old.b * c[2][2] );
}

void sepia_filter( struct image* img ){
  uint32_t x,y;
  for( x = 0; x < img->height; x++ )
    for( y = 0; y < img->width; y++ ){
      struct pixel pxl = get_pixel( img, x, y );
      sepia_one( &pxl );
      set_pixel( img, x, y, pxl );
    }

}

extern void sepia_filter_asm( struct pixel[static 4] );

void sepia_filter_asm_impl( struct image* img ){
  for( size_t x = 0; x < img->width * img->height / 4; sepia_filter_asm( img->pixels + 4 * x ), x++ );
  for( size_t x = (img->width * img->height / 4) * 4; x < img->width * img->height; sepia_one( img->pixels + x ), x++ );
}
