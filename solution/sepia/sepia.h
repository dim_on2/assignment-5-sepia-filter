#ifndef _SEPIA_
#define _SEPIA_
#include "../lib/include/image.h"

void sepia_filter( struct image* img );
void sepia_filter_asm_impl( struct image* img );

#endif
