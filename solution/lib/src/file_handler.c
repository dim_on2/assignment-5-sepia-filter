#include "../include/file_handler.h"

const char* io_message[] = {
  [OPEN_ERR] = "Error occured when file has tried open.",
  [CLOSE_ERR] = "Error occured when file has tried close."
};

enum status_io file_open( FILE** const in, const char* file_name, const char* mode ){
  return (*in = fopen( file_name, mode ), *in) ? OPEN_OK : OPEN_ERR;
}

enum status_io file_close( FILE* const out ){
  return ( fclose( out ) == EOF ) ? CLOSE_ERR : CLOSE_OK;
}
