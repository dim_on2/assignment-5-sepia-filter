#include "../include/image.h"

static struct maybe_image null_image = { .valid = false, .image = {0} };

struct maybe_image create_image( size_t width, size_t height ){
  struct pixel* pixels = malloc( sizeof( struct pixel ) * width * height );
  if ( !pixels ) return null_image;
  return ( struct maybe_image ) { .valid = true, 
                                  .image = ( struct image ) { .pixels = pixels,
                                                              .height = height,
                                                              .width = width } };
}

size_t get_size( const struct image picture ){
  return ( picture.width * picture.height * sizeof( struct pixel ) );
}

void clear_picture_memory( struct image picture ){
  if ( picture.pixels ) free( picture.pixels );
}

struct pixel get_pixel( const struct image* picture, size_t row, size_t col ){
  return ( picture->pixels[ row * picture->width + col ] );
}

void set_pixel( struct image* picture, size_t row, size_t col, struct pixel pixel ){
  picture->pixels[ row * picture->width + col ] = pixel; 
}
