#include "../include/transform_bmp.h"

#define HEADER_SIZE 40
#define PLANES 1
#define BIT_AMOUNT 24
#define NONE_COMPRESSION 0
#define PIXEL_PER_METER_X 2834
#define PIXEL_PER_METER_Y 2834
#define IMPORTANT_ALL_COLOR 0
#define USE_ALL_COLOR 0
#define BM 0x4d42

const char* output_message[] = {
  [READ_INVALID_HEADER] = "Not valid header",
  [WRITE_ERROR] = "Can't write picture",
  [CANT_CREATE_PICTURE] = "Can't create picture"
};

static size_t read_header( FILE* const in, struct bmp_header* const  header ){
  return fread( header, sizeof( struct bmp_header ), 1, in );
}

static bool read_image(  FILE* const in, struct image* const image, uint8_t padding ){
  size_t rows = image->height;
  size_t count_of_read_char = 0;
  for ( size_t i = 0; i < rows; i++ ){
    count_of_read_char += fread( image->pixels + i * image->width, image->width * sizeof( struct pixel ), 1, in );
    if ( fseek( in, padding, SEEK_CUR ) != 0 ) return false;
  }
  return ( count_of_read_char == rows );
} 

static bool write_image(  FILE* const out, const struct image* const image, uint8_t padding ){
  size_t rows = image->height;
  size_t count_of_write_char = 0;
  for ( size_t i = 0; i < rows; i++ ){
    count_of_write_char += fwrite( image->pixels + i * image->width, image->width * sizeof( struct pixel ), 1, out );
    if ( fseek( out, padding, SEEK_CUR ) != 0 ) return false;
  }
  return ( count_of_write_char == rows );
}

static uint8_t calc_padding( uint8_t width_of_picture ){
  return ( 4 - (width_of_picture * sizeof( struct pixel )) % 4 ) % 4;
}

static struct bmp_header new_header( const struct image picture )
{
  return (struct bmp_header) {
    .bfType = BM,
    .bfReserved = 0,
    .bOffBits = sizeof( struct bmp_header ),
    .biSize = HEADER_SIZE,
    .biWidth = picture.width,
    .biHeight = picture.height,
    .biPlanes = PLANES,
    .biBitCount = BIT_AMOUNT,
    .biCompression = NONE_COMPRESSION,
    .biSizeImage = get_size( picture ),
    .bfileSize = get_size( picture ) + sizeof( struct bmp_header ),
    .biXPelsPerMeter = PIXEL_PER_METER_X,
    .biYPelsPerMeter = PIXEL_PER_METER_Y,
    .biClrUsed = USE_ALL_COLOR,
    .biClrImportant = IMPORTANT_ALL_COLOR
  };
}

enum read_status from_bmp( FILE* const in, struct image* const  source_picture ){
  struct bmp_header header = {0};
  size_t num_of_read_char_header = read_header( in, &header );
  if ( num_of_read_char_header == 0 ) return READ_INVALID_HEADER;
  struct maybe_image in_image = create_image( header.biWidth, header.biHeight );
  if ( !in_image.valid ) return CANT_CREATE_PICTURE;
  return ( *source_picture = in_image.image, !read_image( in, source_picture, calc_padding( header.biWidth ) ) ) ? READ_INVALID_BITS : READ_OK;
}

enum write_status to_bmp( FILE* const out, struct image const* const img ){
  struct bmp_header header = new_header( *img );
  if ( fwrite( &header, sizeof( struct bmp_header ), 1, out ) == 0 ) return WRITE_ERROR_HEADER;
  return ( !write_image( out, img, calc_padding( header.biWidth ) ) ) ? WRITE_ERROR_IMAGE : WRITE_OK;
}
