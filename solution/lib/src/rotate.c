#include "../include/rotate.h"

enum allocate_status rotate( struct image const src, struct image* dest){
  size_t rows = src.height;
  size_t cols = src.width;
  struct maybe_image maybe_image = create_image( rows, cols );
  if ( !maybe_image.valid ) return ALLOCATE_ERROR;
  *dest = maybe_image.image;
  for ( size_t i = 0; i < rows; i++ ){
    for ( size_t j = 0; j < cols; j++ ){
      set_pixel( dest, j, dest->width-i-1, get_pixel( &src, i, j ) );
    }
  }
  return ALLOCATE_OK;
}
