#ifndef _transform_bmp_
#define _transform_bmp_
#include "image.h"
#include <stdint.h>
#include <stdio.h>

extern const char* output_message[]; 

enum read_status  {
  READ_OK = 0,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  CANT_CREATE_PICTURE
};

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR,
  WRITE_ERROR_HEADER, 
  WRITE_ERROR_IMAGE
};

struct bmp_header 
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
}  __attribute__((packed));


enum read_status from_bmp( FILE* const in, struct image* const source_picture );
enum write_status to_bmp( FILE* const out, struct image const* img );

#endif
