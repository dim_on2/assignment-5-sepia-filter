#ifndef _rotate_
#define _rotate_
#include "image.h"

enum allocate_status rotate( struct image const src, struct image* dest);

#endif
