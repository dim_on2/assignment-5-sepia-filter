#ifndef _file_handler_
#define _file_handler_
#include <stdint.h>
#include <stdio.h>

enum status_io{
  OPEN_OK = 0,
  OPEN_ERR,
  CLOSE_OK,
  CLOSE_ERR
};

extern const char* io_message[];

enum status_io file_open( FILE** const in, const char* const file_name, const char* const mode );
enum status_io file_close( FILE* const out );

#endif
