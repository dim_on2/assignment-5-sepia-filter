NAME := sepia_filter

CC = gcc
LINKER = $(CC)

ASM = nasm
ASMFLAGS = -felf64 
LDFLAGS = -no-pie

RM = rm -rf
MKDIR = mkdir -p

SRCDIR = solution
OBJDIR.c = obj/c
OBJDIR.asm = obj/asm
BUILDDIR = build

SOURCES.c 	+= $(wildcard $(SRCDIR)/*/*.c) $(wildcard $(SRCDIR)/*/*/*.c)
SOURCES.asm += $(wildcard $(SRCDIR)/*/*.asm)
TARGET  := $(BUILDDIR)/$(NAME)


# $(1) - asm or c
define make-compile

OBJECTS.$(1) := $$(SOURCES.$(1):$$(SRCDIR)/%.$(1)=$$(OBJDIR.$(1))/%.o)
OBJECTS += $$(OBJECTS.$(1))
SRCDEPS.$(1) := $$(OBJECTS.$(1):%.o=%.o.d)

DIRS.$(1) := $$(sort $$(dir $$(OBJECTS.$(1)) $$(TARGET)))
DIRS += $$(DIRS.$(1))


$$(OBJDIR.$(1))/%.o: $$(SRCDIR)/%.$(1) | $$(DIRS.$(1))
ifeq ($(1),c)
	$(CC) $(CFLAGS) -M -MP $$< >$$@.d
	$(CC) $(CFLAGS) -c $$< -o $$@	
else
	$(ASM) $(ASMFLAGS) -o $$@ $$<
endif

-include $$(SRCDEPS)

endef

$(foreach target,asm c,$(eval $(call make-compile,$(target))))

build:	$(TARGET)

.PHONY = build

$(TARGET): $(OBJECTS) | $(DIRS)
	$(LINKER) $(LDFLAGS) $(OBJECTS) -o $@ 

$(sort $(DIRS)):
	$(MKDIR) $@